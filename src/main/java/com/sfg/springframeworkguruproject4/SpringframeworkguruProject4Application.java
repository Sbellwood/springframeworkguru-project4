package com.sfg.springframeworkguruproject4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringframeworkguruProject4Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringframeworkguruProject4Application.class, args);
    }

}
